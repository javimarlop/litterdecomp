data<-read.table('sh_sorb_aran_days.csv',sep=',',header=T)
shsorbas<-data.frame(rep(data$shsorbas,each=24))
sharan<-data.frame(rep(data$sharan,each=24))

write.table(shsorbas,'sorbasaranmod_Data_shsorbas.csv',sep=',',row.names=F)

write.table(sharan,'sorbasaranmod_Data_sharan.csv',sep=',',row.names=F)
